Thomas A. Keller
CS 2 Othello Project

As a solo team I got alpha-beta pruning implemented and working so that
I could increase the look-ahead depth from ~2 to ~6. This was high enough
to beat ConstantTimePlayer and BetterPlayer consistently. I tried to create a variable scoring hueristic based on adjacent squares and 
occupancy of corners, but I could not get it working in time and I have
to work on other finals =/. Hopefully I can come back to it all during break! Thanks for the fun project.
