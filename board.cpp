#include "board.h"

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);

    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            if (isEdge(i, j))
            {
                if(isCorner(i, j))
                    space_value[i][j] = 3;
                else if (isNearCorner(i, j))
                    space_value[i][j] = -2;
                else
                    space_value[i][j] = 2;
            }
            else if (isNearCorner(i, j))
                space_value[i][j] = -3;
            else
                space_value[i][j] = 1;
        }
    }
}


// space_value = {
//      3, -2,  2,  2,  2,  2, -2,  3,
//     -2, -3,  1,  1,  1,  1, -3, -2, 
//      2,  1,  1,  1,  1,  1,  1,  2, 
//      2,  1,  1,  1,  1,  1,  1,  2, 
//      2,  1,  1,  1,  1,  1,  1,  2, 
//      2,  1,  1,  1,  1,  1,  1,  2, 
//     -2, -3,  1,  1,  1,  1, -3, -2, 
//      3, -2,  2,  2,  2,  2, -2,  3
// };


bool isCorner(int x, int y)
{
    if (x % 7 == 0 && y % 7 == 0)
        return true;
    return false;
}

bool isNearCorner(int x, int y)
{
    // if x is an edge and y is next to corner
    if (x % 7 == 0 && y % 5 == 1)
        return true;
    // if x is next to corner and y is an edge
    if (x % 5 == 1 && y % 7 == 0)
        return true;
    // if x is next to corer and y is next to corner
    if (x % 5 == 1 && y % 5 == 1)    
        return true;

    return false;
}

bool isEdge(int x, int y)
{
    if (x % 7 == 0 || y % 7 == 0)
        return true;
    return false;
}
       
/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}


/*
 * Returns an integer score heuristic for the given move on the current board.
 * Expects the given move to be a valid move.
 */
int Board::scoreMove(Move *move, Side side, bool testingMinimax)
{    
    // Copy the current board and try the given move.
    Board *next_board = copy();
    next_board->doMove(move, side);

    return next_board->scoreBoard(side, testingMinimax);
}

/*
 * Returns an integer score heuristic for the given move on the current board.
 * Expects the given move to be a valid move.
 */
vector<Move_Score> Board::getAvailMoves(Side side)
{    
    vector<Move_Score> availMoves;
    Move_Score current_move;

    // Copy the current board and try the given move.
    for (int i = 0; i < 8; i++) 
    {
        for (int j = 0; j < 8; j++) 
        {
            Move move(i, j);
            if (checkMove(&move, side))
            {
                current_move.move = move;
                current_move.score = scoreBoard(side, false);
                availMoves.push_back(current_move);
            }
        }
    }

    sort(availMoves.begin(), availMoves.end());

    return availMoves;
}

/*
 * Returns an integer score heuristic for the entire board.
 */
int Board::scoreBoard(Side side, bool testingMinimax)
{
    Side other = (side == BLACK) ? WHITE : BLACK;

    if (testingMinimax || !hasMoves(side))
        return count(side) - count(other);

    int score = 0;

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (get(side, i, j))
            {
                score += space_value[i][j];
            }
            else if (get(other, i, j))
            {
                score -= space_value[i][j];
            }
        }
    }

    return score;
}


/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}
