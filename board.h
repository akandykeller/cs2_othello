#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include <vector>      
#include <algorithm> 
#include "common.h"
using namespace std;

class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;    

    int space_value[8][8];

    bool occupied(int x, int y);
    bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);
      
public:
    Board();
    ~Board();
    Board *copy();
        
    bool isDone();
    bool hasMoves(Side side);
    bool checkMove(Move *m, Side side);
    void doMove(Move *m, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();

    int scoreMove(Move *m, Side side, bool testingMinimax);
    int scoreBoard(Side side, bool testingMinimax);
    vector<Move_Score> getAvailMoves(Side side);

    void setBoard(char data[]);
};

bool isCorner(int x, int y);
bool isNearCorner(int x, int y);
bool isEdge(int x, int y);

#endif
