#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <climits>
#include "common.h"
#include "board.h"

using namespace std;

class Player {

private:
	Side player_side, opp_side;

public:
    Player(Side side);
    ~Player();
    
  	Move *next_move;
    Move *doMove(Move *opponentsMove, int msLeft);
    Move *randomValidMove();
	Move *maxScoreMove();

	Move_Score getOptimalMove(Board *current_board, Side side, int depth);
    Move_Score getAlphaBetaMove(Board *current_board, Side side, int depth, int alpha, int beta);

	Board *board;

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};


#endif
