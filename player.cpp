/*
 * Thomas A. Keller
 * CS 2 Othello AI
 * March 2014
 */

#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    board = new Board();
    player_side = side; 
    opp_side = (side == BLACK) ? WHITE : BLACK; 
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    // Maximum lookahead depth
    int max_depth = 8;
    
    // Record the opponent's move onto the board
    board->doMove(opponentsMove, opp_side);

    next_move = new Move(-1, -1);

    // If the current player cannot make a move
    if (!board->hasMoves(player_side))
    {
        next_move = NULL;
    }
    else
    { 
        if (testingMinimax)
            max_depth = 2;

        Move_Score next_move_score = getAlphaBetaMove(board, player_side, max_depth, INT_MIN, INT_MAX);
        *next_move = next_move_score.move;
        //fprintf(stderr, "move: %d, %d\n", next_move->getX(), next_move->getY());
        if (next_move->getX() == -1 && next_move->getY() == -1)
            next_move = NULL;
    }

    board->doMove(next_move, player_side);
    return next_move;
}


/*
 * Returns a random valid move for the player givent the current board.
 * Assumes that a valid move exists for the player.
 */
Move *Player::randomValidMove() {
    Move *random_move = new Move(-1, -1);

    do
    {
        random_move->setX(rand() % 8);
        random_move->setY(rand() % 8);
    } while (!board->checkMove(random_move, player_side));

    return random_move;
}


/*
 * Returns the next move which will result in the maximum score for the
 * current player with respect to the board scoring heuristic from scoreMove. 
 */
Move *Player::maxScoreMove() {
    int score;
    // Set the initial maximum score to -64 since this is the value if 
    // all spaces are lost
    int max_score = -64;
    Move *best_move = new Move(-1, -1);

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (board->checkMove(&move, player_side))
            {
                score = board->scoreMove(&move, player_side, testingMinimax);
                if (score >= max_score)
                {
                    max_score = score;
                    *best_move = move; 
                }
            }
        }
    }

    return best_move;
}

/*
 * A recursive function which returns the optimal move and score for that 
 * move given a current board, a side, and a recursion depth.
 */
Move_Score Player::getOptimalMove(Board *current_board, Side side, int depth) {
    Side other = (side == BLACK) ? WHITE : BLACK;

    int best_score;
    Move *best_move = new Move(-1, -1);
    Move_Score best_move_score;

    // Set the current best found score to respective limits for finding 
    // the max when we are at a player's decision node, and for finding
    // a minimum when we are at the opponent's decision node.
    if (side == player_side)
        best_score = INT_MIN;
    else
        best_score = INT_MAX;

    // If we have reached our max recursion depth, return the score
    // of the current board with a NULL move.
    if (depth == 0 || !current_board->hasMoves(side))
    {
        best_move_score.move = *best_move;
        best_move_score.score = current_board->scoreBoard(side, testingMinimax);
    }
    else
    { 
        for (int i = 0; i < 8; i++) 
        {
            for (int j = 0; j < 8; j++) 
            {
                Move move(i, j);
                if (current_board->checkMove(&move, side))
                {
                    Board * next_board = current_board->copy();
                    next_board->doMove(&move, side);

                    best_move_score = getOptimalMove(next_board, other, depth - 1);
                    delete next_board;

                    if (side == player_side && best_move_score.score > best_score)
                    {
                        best_score = best_move_score.score;
                        *best_move = move;
                    }
                    else if (side != player_side && best_move_score.score < best_score)
                    {
                        best_score = best_move_score.score;
                        *best_move = move;
                    }
                }
            }
        }

        best_move_score.move = *best_move;
        best_move_score.score = best_score;
    }

    return best_move_score;
}


/*
 * A recursive function which returns the optimal move and score for that 
 * move given a current board, a side, and a recursion depth.
 */
Move_Score Player::getAlphaBetaMove(Board *current_board, Side side, int depth, int alpha, int beta) {
    Side other = (side == BLACK) ? WHITE : BLACK;

    int best_score;
    Move current_move;
    Move *best_move = new Move(-1, -1);
    Move_Score best_move_score;
    vector<Move_Score> availMoves;

    // Set the current best found score to respective limits for finding 
    // the max when we are at a player's decision node, and for finding
    // a minimum when we are at the opponent's decision node.
    if (side == player_side)
        best_score = INT_MIN;
    else
        best_score = INT_MAX;

    // If we have reached our max recursion depth, return the score
    // of the current board with a NULL move.
    if (depth == 0 || !current_board->hasMoves(side))
    {
        best_move_score.move = *best_move;
        best_move_score.score = current_board->scoreBoard(side, testingMinimax);
        return best_move_score;
    }
    else
    { 
        availMoves = current_board->getAvailMoves(side);

        //fprintf(stderr, "Found %d available moves\n", availMoves.size());

        if (side == player_side)
        {
            for(vector<Move_Score>::iterator it = availMoves.begin(); it != availMoves.end(); ++it)
            {
                current_move = (*it).move;
                //fprintf(stderr, "Trying player move (%d, %d)\n", current_move.getX(), current_move.getY());

                Board * next_board = current_board->copy();
                next_board->doMove(&current_move, side);

                best_move_score = getAlphaBetaMove(next_board, other, depth - 1, alpha, beta);
                delete next_board;

                alpha = max(alpha, best_move_score.score);
                        
                if (best_move_score.score > best_score)
                {
                    *best_move = current_move;
                    best_score = best_move_score.score;
                }

                if (beta <= alpha)
                {
                    //fprintf(stderr, "Beta cutoff pruning: beta: %d, alpha: %d\n", beta, alpha);
                    best_move_score.move = current_move;
                    best_move_score.score = alpha;
                    return best_move_score;
                }
            }
        }

        else
        {
            for(vector<Move_Score>::iterator it = availMoves.begin(); it != availMoves.end();  ++it)
            {
                current_move = (*it).move;
                //fprintf(stderr, "Trying opponent move (%d, %d)\n", current_move.getX(), current_move.getY());

                Board * next_board = current_board->copy();
                next_board->doMove(&current_move, side);

                best_move_score = getAlphaBetaMove(next_board, other, depth - 1, alpha, beta);
                delete next_board;

                beta = min(beta, best_move_score.score);
                        
                if (best_move_score.score < best_score)
                {
                    *best_move = current_move;
                    best_score = best_move_score.score;
                }

                if (beta <= alpha)
                {
                  //  fprintf(stderr, "Alpha cutoff pruning: beta: %d, alpha: %d\n", beta, alpha);
                    best_move_score.move = current_move;
                    best_move_score.score = beta;
                    return best_move_score;
                }

            }
        }
    }

    best_move_score.move = *best_move;
    best_move_score.score = best_score;

    return best_move_score;
}

